import os
import shutil
import urllib.request
import urllib.parse
from datetime import datetime
from pathlib import Path
from .helpers import run_command
from .worker import app


HOSTNAME = os.getenv('HOSTNAME', 'celery-worker')
APP_PATH = Path(os.getenv('APP_PATH', '/app'))
APP_SCRATCH = Path(os.getenv('APP_SCRATCH', '/scratch')) / 'bionode'
KEEP_APP_SCRATCH_DAYS = int(os.getenv('KEEP_APP_SCRATCH_DAYS', 7))
API_BIN = 'bionodeapi'
API_TOKEN = os.getenv('BIONODE_API_TOKEN', 'null')


@app.task(bind=True, name='cron', queue='beat')
def cron(self, tasks):  
    for task in tasks:
        print(f'got task: {task}')
        if task == 'cleanup':
            for sfolder in APP_SCRATCH.glob('*'):
                createTime = datetime.fromtimestamp(sfolder.stat().st_atime)
                delta = datetime.now() - createTime
                if delta.days > KEEP_APP_SCRATCH_DAYS:
                    shutil.rmtree(sfolder)
        elif task == 'beat':
            print(f'Got beat: {HOSTNAME}')


@app.task(bind=True, name='bionode.submit')  
def submit(self, *args, **kwargs):
    
    # process taks
    print(f'New bionode submission: args: [{", ".join(args)}], kwargs: [{ ", ".join( [ "%s=%s" % (i,j) for i, j in kwargs.items() ] ) }]')
    
    sid = kwargs['submission_id']
    
    tmp_inputs_dir_base = APP_SCRATCH / sid / 'inputs'
    tmp_inputs_dir_base.mkdir(parents=True)

    tmp_outputs_dir_base = APP_SCRATCH / sid / 'outputs'
    tmp_outputs_dir_base.mkdir(parents=True)

    for job in kwargs['jobs']:
        input_idx = 0
        for file in job['files']:
            input_mapped = kwargs['io_mapping']['inputs'][input_idx]
            tmp_inputs_dir_job = tmp_inputs_dir_base / str(input_mapped) / str(job['dbid'])
            tmp_inputs_dir_job.mkdir(parents=True)
            file_path = Path(file)
            (tmp_inputs_dir_job / file_path.name).symlink_to(file_path)
            input_idx += 1
        input_mapped = kwargs['io_mapping']['inputs'][input_idx]
        tmp_inputs_dir_job = tmp_inputs_dir_base / str(input_mapped) / str(job['dbid'])
        tmp_inputs_dir_job.mkdir(parents=True)
        mifaser_config_file = tmp_inputs_dir_job / "mifaser_config.txt"
        with mifaser_config_file.open('w') as fout:
            fout.write(" ".join(str(f) for f in job['flags']))

    run_command(
        [
            # API_BIN, kwargs['api-key'], kwargs['inputs-dir'], kwargs['outputs-dir']
            API_BIN, kwargs['api-key'], tmp_inputs_dir_base, tmp_outputs_dir_base
        ],
        env_exports = {
            "BIONODE_DOMAIN": "https://bio-no.de",
            "TOKEN": API_TOKEN,
            "BIONODE_SYMLINK": "1"
        }
    )

    status = "ok"

    try:
        url_api = f'http://webserver/api/v1/celery/callback/{sid}:{status}'
        req = urllib.request.Request(url_api)
        urllib.request.urlopen(req)
    except Exception as err:
        print(f'[Exception] Error for [{sid}]: {err}')
