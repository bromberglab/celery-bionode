import os
from celery import Celery

app = Celery(include=('bionode.celery.tasks',))
app.conf.beat_schedule = {
    'cron': {
        'task': 'cron',
        'schedule': float(os.environ['BEAT_SCHEDULE']),
        'args': (["beat"],)
    },
    'cleanup': {
        'task': 'cron',
        'schedule': float(os.environ['CLEANUP_SCHEDULE']),
        'args': (["cleanup"],)
    },
}
